import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { StepsComponent } from './steps.component';
import { MatStepperModule } from '@angular/material/stepper';
import { FormsModule } from '@angular/forms';

const routes: Routes = [{
  path: 'steps',
  component: StepsComponent
}];

@NgModule({
  declarations: [
    StepsComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatStepperModule,
    FormsModule
  ],
  exports: [
    StepsComponent,
  ],
  providers: [],

})
export class StepsModule {
}
