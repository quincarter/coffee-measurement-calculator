import { Common } from '../../classes/common';
import { Component } from '@angular/core';

export class CoffeeMeasurements {
  constructor(
    public gramsOfWater: number,
    public gramsOfCoffee: number,
    public goalOunces: number
  ) {
  }
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {

  // defining default values for the form
  model = new CoffeeMeasurements(null, null, null);

  submitted = false;

  calculateCoffeeGrams(): void {
    this.model.gramsOfCoffee = Common.getGramsOfCoffeeBasedOnWater(this.model.gramsOfWater);
    this.model.gramsOfCoffee = parseFloat(this.model.gramsOfCoffee.toFixed(2));
  }

  calculateWaterGrams(ounces: number): void {
    this.model.gramsOfWater = Common.getGramsFromOunces(ounces);
    this.calculateCoffeeGrams();
    this.model.gramsOfWater = Math.round(this.model.gramsOfWater);
  }

  calculateWaterOunces(grams: number): void {
    this.model.goalOunces = Common.getOuncesFromGrams(grams);
    this.calculateCoffeeGrams();
    this.model.goalOunces = parseFloat(this.model.goalOunces.toFixed(2));
  }

  onSubmit() {
    this.submitted = true;
  }

  edit(): void {
    this.submitted = false;
  }
}
