import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatInputModule } from '@angular/material/input';
import { LayoutModule } from '@angular/cdk/layout';
import { StepsModule } from '../steps/steps.module';
import { FormsModule } from '@angular/forms';
import { FlexModule } from '@angular/flex-layout';

const routes: Routes = [{
  path: '',
  component: DashboardComponent
}];

@NgModule({
  declarations: [
    DashboardComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    MatGridListModule,
    MatCardModule,
    LayoutModule,
    RouterModule.forChild(routes),
    StepsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    FlexModule
  ],
  exports: [
    DashboardComponent,
  ],
  providers: [],

})
export class DashboardModule {
}
