export class Common {

  // 1 gram = 0.035274 ounces
  // 1 ounce = 28.3495 grams
  // 1 gram of coffee = 16.666666667 grams of water

  public static OUNCES_BASE = 0.035274;
  public static GRAMS_BASE = 28.3495;
  public static COFFEE_GRAMS_BASE = 16.666666667;

  public static getOuncesFromGrams(grams: number): number {
    return grams * this.OUNCES_BASE;
  }

  public static getGramsFromOunces(ounces: number): number {
    return ounces * this.GRAMS_BASE;
  }

  public static getGramsOfCoffeeBasedOnWater(gramsWater: number): number {
    return gramsWater / this.COFFEE_GRAMS_BASE;
  }
}
